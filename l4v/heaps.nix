{ stdenv, isabelle, hostname, perl }:

stdenv.mkDerivation {
  name = "initial-heaps";

  phases = [ "buildPhase" ];

  nativeBuildInputs = [
    isabelle hostname
    # TODO
    (perl.nativeDrv or perl)
  ];

  buildPhase = ''
    export HOME=$out
    isabelle build -vb Pure
    isabelle build -vb HOL
    isabelle build -vb HOL-Word
  '';
}
