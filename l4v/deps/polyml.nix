{ stdenv, lib, fetchFromGitHub, autoreconfHook, gmp, libffi }:

stdenv.mkDerivation rec {
  pname = "polyml";
  version = "5.8";

  src = fetchFromGitHub {
    owner = "SEL4PROJ";
    repo = "polyml";
    rev = "03c494fd4ef6c474b972efb19bebe12058c86651";
    sha256 = "0xzx1dvfz4x61pv22ylc8dglpzgagp9nfzi5wl0wcikmf8fsa263";
  };

  buildInputs = [ libffi gmp ];

  configureFlags = [
    "--enable-shared"
    "--with-system-libffi"
    "--with-gmp"
  ];

}
