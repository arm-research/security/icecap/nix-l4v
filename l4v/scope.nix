{ texlive }:

self: with self;

{

  armv7Pkgs = import ../nix-isabelle/nixpkgs {
    crossSystem = {
      system = "armv7l-linux";
      config = "armv7l-unknown-linux-gnueabi";
    };
  };

  polyml = callPackage ./deps/polyml.nix {};
  hol4 = callPackage ./deps/hol4.nix {};

  texlive-env = with texlive; combine {
    inherit
      collection-fontsrecommended
      collection-latexextra
      collection-metapost
      collection-bibtexextra;
  };

  
  source = callPackage ./source.nix {};
  graph-refine = callPackage ./graph-refine.nix {};

  initial-heaps = callPackage ./heaps.nix {};

  env = callPackage ./env.nix {};
  specs = callPackage ./specs.nix {};
  tests = callPackage ./tests.nix {};

}
