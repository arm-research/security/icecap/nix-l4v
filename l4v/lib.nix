{ lib, splicePackages, __splicedPackages, pkgs }:

rec {

  mkScopeWithFrom = superSplicedScope: superSpliceArgs: access: f:
    let
      spliceArgs = lib.mapAttrs (k: v: access v) superSpliceArgs;
      splicedScope = superSplicedScope // splicePackages spliceArgs;
      self = f self // {
        mkScopeWith = mkScopeWithFrom splicedScope spliceArgs;
        callPackage = lib.callPackageWith splicedScope;
      };
    in self;

  mkScopeWith = mkScopeWithFrom __splicedPackages {
    inherit (pkgs)
      pkgsBuildBuild pkgsBuildHost pkgsBuildTarget
      pkgsHostHost pkgsHostTarget
      pkgsTargetTarget
      ;
  };

}
