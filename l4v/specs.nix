{ pkgs, lib, hostPlatform, fetchFromGitHub, writeScriptBin, runtimeShell, buildPackages
, python2Packages, source, isabelle, texlive-env
, rsync, git, perl, hostname
}:

let
  cpp-wrapper = writeScriptBin "cpp" ''
    #!${runtimeShell}
    export L4CPP="-DTARGET=ARM -DTARGET_ARM -DPLATFORM=Sabre -DPLATFORM_Sabre"
    clang -E -x c $@ \
      -Wno-invalid-pp-token -Wno-unused-command-line-argument
  '';

in
pkgs.stdenv.mkDerivation {
  name = "sel4-specs";

  src = fetchFromGitHub {
    owner = "sel4";
    repo = "l4v";
    rev = "52dae5f25abcc6bd3a2e70bb35c19ec421849b69";
    sha256 = "1rrfcvnr1d6rssnwviq2nkkwq8c26rsg978pi3ijpal109543gph";
  };

  patches = [
    ./patches/no-git.patch
  ];

  depsBuildBuild = [
    (if hostPlatform.isDarwin then cpp-wrapper else buildPackages.stdenv.cc)
  ];

  nativeBuildInputs = [
    texlive-env python2Packages.sel4-deps
    rsync git hostname
    # TODO ?
    (perl.nativeDrv or perl)
  ];

  postPatch = ''
    substituteInPlace spec/Makefile \
      --replace SEL4_VERSION=../../seL4/VERSION SEL4_VERSION=${source}/seL4/VERSION

    patchShebangs .
  '';

  configurePhase = ''
    export HOME=$NIX_BUILD_TOP/home
    mkdir $HOME

    rm isabelle
    ln -s ${isabelle.nativeDrv or isabelle} isabelle
  '';

  buildPhase = ''
    mkdir $out
    cd spec

    do_arch() {
      L4V_ARCH=$1 make ASpecDoc
      cp -v $HOME/.isabelle/browser_info/Specifications/ASpecDoc/document.pdf $out/$1.pdf
    }

    do_arch ARM
    do_arch ARM_HYP
    do_arch RISCV64
    do_arch X64
  '';

  dontInstall = true;
}
