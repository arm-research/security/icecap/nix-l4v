{ mkShell, pkgs
, isabelle, polyml, hol4, texlive-env, graph-refine
}:

mkShell {
  name = "env";

  nativeBuildInputs = [
    isabelle polyml hol4
    texlive-env
  ] ++ (with pkgs; [
    mlton
    haskellPackages.stack
    python2Packages.sel4-deps
    cmake ninja ccache
    curl dtc
    zlib libxml2 ncurses
  ]);

  GRAPH_REFINE = graph-refine;

}
