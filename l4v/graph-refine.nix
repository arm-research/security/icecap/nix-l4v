{ stdenv, lib, fetchFromGitHub
, python2Packages
}:

stdenv.mkDerivation {
  name = "sel4-graph-refine";

  src = fetchFromGitHub {
    owner = "SEL4PROJ";
    repo = "graph-refine";
    rev = "debb3352b9740f013fbff390547f5cf5021a224d";
    sha256 = "0qym0hz50dx0lfrydi8p1yvr6c3n8ga9bhv33k3bls8bncmvc8sc";
  };

  nativeBuildInputs = [
    python2Packages.python
  ];

  phases = [ "unpackPhase" "patchPhase" "installPhase" ];

  patchPhase = ''
    patchShebangs .
  '';

  installPhase = ''
    mkdir $out
    mv * $out
  '';

}
