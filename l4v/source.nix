{ stdenv, lib, fetchFromGitHub, runCommand
, python2Packages, cmake
, isabelle, armv7Pkgs
, graph-refine
}:

let

  l4v = stdenv.mkDerivation {
    name = "l4v-source";

    src = fetchFromGitHub {
      owner = "sel4";
      repo = "l4v";
      rev = "f053fb01815c2afe90c7cb4d476e82713fea3674";
      sha256 = "1f76vln9ja9zs55m094n2i9ib03226ham2rfg1nbnjz3h2949wqs";
    };

    phases = [ "unpackPhase" "patchPhase" "installPhase" ];

    nativeBuildInputs = [
      python2Packages.python
    ];

    patches = [
      ./patches/no-stack.patch
      ./patches/no-git.patch
      ./patches/fix-psutil-check.patch
      ./patches/cabal.patch
    ];

      # cpp_files="
      #   tools/c-parser/isar_install.ML
      #   tools/c-parser/standalone-parser/tokenizer.sml
      #   tools/c-parser/standalone-parser/main.sml
      #   tools/c-parser/testfiles/jiraver313.thy
      #   "
      # for x in $cpp_files; do
      #   substituteInPlace $x --replace /usr/bin/cpp ${stdenv.cc}/bin/cpp
      # done

      # echo_files="
      #   tools/c-parser/tools/mlyacc/Makefile
      #   tools/c-parser/tools/mllex/Makefile
      #   "
      # for x in $echo_files; do
      #   substituteInPlace $x --replace /bin/echo echo
      # done

    postPatch = ''
      patchShebangs .
    '';

    installPhase = ''
      mkdir $out
      mv * $out
    '';
  };

  sel4 = stdenv.mkDerivation {
    name = "sel4-source";

    src = fetchFromGitHub {
      owner = "sel4";
      repo = "sel4";
      rev = "8748d8ea5eb59a0695c806e441c8488b237757c5";
      sha256 = "1dsn4m9paq947sm2zrq7nx8ikvn0abzln9da0833w2d4a6zyrqmg";
    };

    nativeBuildInputs = [
      python2Packages.python
    ];

    phases = [ "unpackPhase" "patchPhase" "installPhase" ];

    patchPhase = ''
      sed -i 's,#!/usr/bin/env -S cmake -P,#!${cmake}/bin/cmake -P,' configs/*_verified.cmake

      patchShebangs .
    '';

    installPhase = ''
      mkdir $out
      mv * $out
    '';
  };

in runCommand "verification-source" {
  passthru = {
    inherit l4v sel4 graph-refine;
  };
} ''
  mkdir $out
  cp -r ${l4v} $out/l4v
  cp -r ${sel4} $out/seL4
  cp -r ${graph-refine} $out/graph-refine
  ln -s ${(isabelle.nativeDrv or isabelle).home} $out/isabelle
''
